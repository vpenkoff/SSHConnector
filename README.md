#SSHConnector
SSHConnector is an utility with a simple graphical front end that wraps up the tedious ssh input.

##1. Purpose
Do you often need to fire up a console (terminal) and type 'ssh -l username host' or similar stuff to connect to a remote host?
I personally need to fire up to 4 or 6 terminals with ssh sessions to multiple hosts multiple times a day... 
This is boring!!! And I'm lazy. Guess what! SSHConnector gives you the chance with one click to fire up your sessions.

##2. Setup
SSHConnector reads a hosts file, which is json based, containing your hosts and usernames. No passwords there.
The structure of the file is:

```
hosts.json:
[
  {
    "hostname": "localhost",
    "username": "joe"
  }
]
```

Moreover, SSHConnector gives you a chance to choose your terminal emulator:
```
app-config.json:
{
  "terminal": "xterm"
}
```


##3. Requirements
SSHConnector depends on the following stuff:
- openssh-client
- python3.5
- gtk3

##4. Installation
First, run ```python3 setup.py sdist```. This will create a *tar.gz* archive, which you could put wherever you want.
Then, extract the archive, edit the two config files, and run ```./sshconnector```. 

###5. TODO:
