import json
import subprocess
from multiprocessing import Process
import shlex


SSH = 'ssh'
APP_CONFIG_PATH = "./config/app-config.json"
HOSTS_PATH = "./config/hosts.json"


def readConfigFile():
  with open(APP_CONFIG_PATH, 'r') as f:
    read_data = f.read()

  return json.loads(read_data)


def readHostFile():
  with open(HOSTS_PATH, 'r') as f:
    read_data = f.read()
  
  return json.loads(read_data)


# could be optimized
def getHosts():
  hosts = []
  hostFile = readHostFile()
  for el in hostFile:
    hosts.append(el)

  return hosts


def connectToHost(hostname, username):
  options = shlex.split(buildOptions(hostname, username))
  p = subprocess.Popen(options)


def buildOptions(hostname, username):
  terminal = readConfigFile()['terminal']
  command = "{0} -e '{1} -l {2} {3}'".format(terminal, SSH, username, hostname)
  return command


def startProc(host):
    p = Process(target=connectToHost, args=(host,))
    p.start()
