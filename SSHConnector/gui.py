import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk
from SSHConnector import utils


# the tree model
class HostListStore():

  def __init__(self, hosts):
    self.store = Gtk.ListStore(str)
    self.__populate(hosts)

  def __populate(self, hosts):
    for el in hosts:
      self.store.append([el['hostname']])


# the tree view
class HostList():

  def __init__(self, model):
    self.view = Gtk.TreeView.new_with_model(model)
    self.__populate()

  def __populate(self):
    renderer = Gtk.CellRendererText()
    column = Gtk.TreeViewColumn("Hosts", renderer, text=0)
    self.view.append_column(column)


# the main window
class MyWindow(Gtk.Window):

  def __init__(self):
    Gtk.Window.__init__(self, title='SSHConnector')
    Gtk.Window.set_position(self, Gtk.WindowPosition.CENTER)

    self.selected_host = None
    self.hosts = utils.getHosts()

    grid = buildGrid()
    self.add(grid)

    model = HostListStore(self.hosts)
    self.hostList = HostList(model.store)

    scrollable_treelist = Gtk.ScrolledWindow()
    scrollable_treelist.set_vexpand(True)
    grid.attach(scrollable_treelist, 0, 0, 5, 5)
    scrollable_treelist.add(self.hostList.view)

    selection = self.hostList.view.get_selection()
    selection.connect("changed", self.on_tree_selection_changed)

    button1 = Gtk.Button.new_with_label("Connect")
    button1.set_tooltip_text("Connect to the selected host")
    button2 = Gtk.Button.new_with_label("Quit")
    button2.set_tooltip_text("Quit the application")

    grid.attach_next_to(button1, scrollable_treelist, Gtk.PositionType.BOTTOM, 1, 1)
    #grid.attach_next_to(button2, button1, Gtk.PositionType.RIGHT, 1, 1)
    grid.attach(button2, 4, 5, 1, 1)

    button1.connect("clicked", self.on_connect_clicked)
    button2.connect("clicked", self.on_close_clicked)

  def on_tree_selection_changed(self, selection):
    model, treeIter = selection.get_selected()
    if treeIter != None:
      self.selected_host = model[treeIter][0]
    else:
      show_error_dialog(self, "Please select host from the list")


  def on_close_clicked(self, button):
    Gtk.main_quit()

  def on_connect_clicked(self, button):
    username = None
    for el in self.hosts:
      if el['hostname'] == self.selected_host:
        password = el['username']
    utils.connectToHost(self.selected_host, password)


def runGui():
  win = MyWindow()
  win.connect("delete-event", Gtk.main_quit)
  win.show_all()
  Gtk.main()


def buildGrid():
  grid = Gtk.Grid()
  grid.set_column_homogeneous(True)
  grid.set_row_homogeneous(True)
  return grid


def show_error_dialog(parent_window, message):
  dialog = Gtk.MessageDialog(parent_window, 0, Gtk.MessageType.ERROR,
      Gtk.ButtonsType.OK, message)
  dialog.run()

