# -*- coding: utf-8 -*-

from distutils.core import setup


setup(
    name='SSHConnector',
    version='0.0.1',
    description='Utility for easily connecting through ssh to remote hosts',
    author='Viktor Penkov',
    author_email='vpenkoff@gmail.com',
    url='https://github.com/vpenkoff/SSHConnector',
    license=license,
    scripts=['sshconnector.py'],
    packages=['SSHConnector'],
    data_files=[
      ('config', ['config/hosts.json', 'config/app-config.json']),
    ]
)
